# Godot & SDL Build Config

This repository builds [Godot](https://gitlab.com/abmyii/ubports-godot) and
[SDL](https://gitlab.com/abmyii/sdl), both patched to work on Ubuntu Touch.

**Credits**: Thanks to abmyii for providing the patches.

This repository utilizes the Gitlab CI to build and provide the artifacts.

## Build Instructions

This build config uses [Clickable](https://clickable-ut.dev/en/latest/) to
compile software in an Ubuntu Touch compatible environment:

```bash
clickable build --libs --arch <amd64|arm64|armhf>
```
